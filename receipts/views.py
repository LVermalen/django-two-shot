from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


@login_required
def show_receipts(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipt_list}

    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            form.instance.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        form.fields["account"].queryset = Account.objects.filter(
            owner=request.user
        )
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=request.user
        )
    context = {"form": form}
    return render(request, "receipts/new.html", context)


@login_required
def show_expense_categories(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": category_list}
    return render(request, "expense_categories/list.html", context)


@login_required
def show_accounts(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {"account_list": account_list}
    return render(request, "accounts/list.html", context)


@login_required
def create_expense_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.instance.owner = request.user
            form.save()
            return redirect("list_categories")
    else:
        form = CategoryForm()
    context = {"form": form}
    return render(request, "expense_categories/new.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            form.instance.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "accounts/new.html", context)
